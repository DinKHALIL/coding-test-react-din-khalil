import React, { Component, Fragment } from "react";
import Helmet from "react-helmet";
import { Link } from "react-router-dom";
import Icon from "@material-ui/core/Icon";

import { title } from "utils";

import { userService } from "services";

//Circular progress
//import CircularProgress from "../components/CircularProgress"

//card
import ArticleCard from "../components/ArticleCard";

//Select
import SelectUser from "../components/SelectUser";

class UserPage extends Component {
  constructor(props) {
    super(props);
    // Init
    this.state = {
      list: [],
      id: 1,
      user: { articles: [] }
    };
  }

  init() {
    //Vérifie si la route contient un id sinon id=1 par default
    if (this.props.match.params.id) {
      //Récuper la liste pour le Select
      userService.list().then(list => {
        this.setState({ list: list });
      });
      //Set de user selon la route /user/id
      userService.userById(this.props.match.params.id).then(user => {
        this.setState({ user: user });
      });
      //Set de l'id selon la route /user/id
      this.setState({ id: this.props.match.params.id });
    } else {
      userService.list().then(list => {
        this.setState({ list: list });
      });
      userService.userById(1).then(user => {
        this.setState({ user: user });
      });
      this.setState({ id: 1 });
    }
  }

  componentDidMount() {
    this.init();
  }

  userChange = event => {
    this.props.history.push("/users/" + event.target.value);
    this.init();
  };

  render() {
    var { id, list, user } = this.state;

    return (
      <Fragment>
        <Helmet>{title("Page secondaire")}</Helmet>

        <div className="user-page content-wrap">
          {/* Icon pour aller a la page d'accueil */}
          <Link to="/" className="nav-arrow">
            <Icon style={{ transform: "rotate(180deg)", fontSize: 90 }}>
              arrow_right_alt
            </Icon>
          </Link>

          <div className="users-select userValue">
            {/* Select */}
            <SelectUser users={list} id={id} userUpdate={this.userChange} />
          </div>

          {/* Saut de page */}
          <br />

          <div className="infos-block">
            {/* Infos dynamiques sur l'utilisateur sélectionné */}
            <p className="userKey">Occupation:</p>{" "}
            <p className="userValue">{user.occupation}</p> <br />
            <p className="userKey">Date de naissance:</p>{" "}
            <p className="userValue">{user.birthdate}</p>
          </div>

          <div className="articles-list">
            {/* Liste dynamique tirée de l'utilisateur sélectionné */}
            <ArticleCard articles={user.articles} />
          </div>
        </div>
      </Fragment>
    );
  }
}

export default UserPage;
