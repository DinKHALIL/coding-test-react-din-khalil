import React, { Component, Fragment } from "react";
import Helmet from "react-helmet";
import { Link } from "react-router-dom";
import Icon from "@material-ui/core/Icon";

import { title } from "utils";

class HomePage extends Component {
  render() {
    return (
      <Fragment>
        <Helmet>{title("Page d'accueil")}</Helmet>

  <span className="dot1"></span>
  <span className="dot2"></span>
        <div className="home-page content-wrap">
          <div className="infos-block">
            <h1 className="title">04h11</h1>
            <p className="subtitle">Le spécialiste de vos données.</p>
          </div>

          {/* Icon pour aller a la suivante */}
          <Link to="/users" className="main-nav-arrow">
            <Icon style={{ fontSize: 90 }}>arrow_right_alt</Icon>
          </Link>
        </div>
      </Fragment>
    );
  }
}

export default HomePage;
