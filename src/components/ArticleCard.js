import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    padding: "50px"
  },
  paper: {
    padding: theme.spacing(5),
    textAlign: "center",
    color: theme.palette.text.secondary,
    fontSize: "1vw",
    height: "14vw",
    overflowY: "scroll"
  },
  mainTitle: {
    fontWeight: "bold"
  }
}));

export default function CenteredGrid(props) {
  const classes = useStyles();
  const articles = props.articles;

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        {articles.map(article => (
          <Grid item xs={6}>
            <Paper className={classes.paper}>
              <h2 className={classes.mainTitle}>{article.name} </h2>
              <br />
              <p>{article.content}</p>
            </Paper>
          </Grid>
        ))}
      </Grid>
    </div>
  );
}
