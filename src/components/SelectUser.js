import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    width: 300,
    borderColor: "white",
    border: "5px solid",
    textAlign: "center"
  },
  select: {
    color: "white"
  },
  icon: {
    fill: "white"
  }
}));


export default function Selectuser(props) {
  const classes = useStyles();
  const users = props.users;
  const id = props.id;
  const userUpdate = props.userUpdate;

  return (
    <div>
      <FormControl variant="outlined" className={classes.formControl}>
        <Select
          value={id}
          className={classes.select}
          onChange={userUpdate}
          inputProps={{
            classes: {
              icon: classes.icon
            }
          }}
        >
          {users.map(user => (
            <MenuItem value={user.id}>
              {user.name} # {user.id}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
}
